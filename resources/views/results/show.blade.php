@extends('layouts.app')

@section('content')

    <div class="row text-center justify-content-md-center">
        <div class="col-md-12">
            <h2>Analysis result for:<br>
                {{$result['checked_url']}}
            </h2>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <p><b>WEB: </b>{{$result['checked_url']}}</p>
            <hr>
            <p><b>STATUS: </b>{{$result['status']}}</p>
            <hr>
            <pre><b>HTTP version:</b>{{$result['protocol']}}</pre>
            <hr>
            <pre><b>GZIP support: </b>@json($result['gzip'],JSON_PRETTY_PRINT)</pre>
            <hr>
            <pre><b>Robots meta tag: </b>{{$result['robots_meta_tag']}}</pre>
            <hr>
            <pre><b>Robots.txt: </b>{{ $result['robots_txt']}}</pre>
            <hr>
            <pre><b>X ROBOTS TAG: </b>@json($result['x_robots_tag'])</pre>
            <hr>
            <p><b>Google Insights result:</b> <br>
                Captcha : @json($result['insights']['captcha'])<br>
                Final url : @json($result['insights']['finalUrl'])<br>
            </p>
            <hr>
            <pre><b>Images with missing alt tag:{{count($result['missing_alt_tags'])}} </b>@json($result['missing_alt_tags'],JSON_PRETTY_PRINT)</pre>
            <hr>
        </div>
    </div>
@endsection