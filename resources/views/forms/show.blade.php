@extends('layouts.app')

@section('content')

    <div class="row text-center justify-content-md-center">
        <div class="col-md-12">
            <h2>Url analysis</h2>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <form method="POST" action="{{route('check.url')}}">
                @csrf
                <div class="form-group">
                    <label for="checked_url">Url:</label>
                    <input type="text" class="form-control" name="checked_url" id="checked_url"
                           placeholder="Insert Url">
                    @if ($errors->has('checked_url'))
                        <p class="alert alert-danger">{{ $errors->first('checked_url') }}</p>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Run analysis</button>
            </form>

        </div>
    </div>
@endsection