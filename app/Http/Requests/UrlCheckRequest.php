<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed checked_url
 */
class UrlCheckRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'checked_url' => 'required|url|active_url',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'checked_url.required' => 'A url address is required',
            'checked_url.url' => 'A url address is not valid',
            'checked_url.active_url' => 'A url address is not valid',

        ];
    }
}
