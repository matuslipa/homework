<?php

namespace App\Http\Controllers;

use App\Http\Requests\UrlCheckRequest;
use DOMDocument;
use Illuminate\Support\Facades\Http;

class UrlController extends Controller
{

    /**
     * @param null $result
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUrlForm($result = null)
    {
        return view('forms.show')->with('result', $result);
    }


    public function checkUrl(UrlCheckRequest $request)
    {
        $url = $request->checked_url;

        $result = $this->getData($url);
        $insights = $this->getInsightsData($url);

        $result['insights']['captcha'] = $insights->captchaResult;
        $result['insights']['finalUrl'] = $insights->lighthouseResult->finalUrl;

        $html = file_get_contents($url);
        $robots = file_get_contents($url . '/robots.txt');
        $result['robots_txt'] = $robots;
        $doc = new DOMDocument();
        @$doc->loadHTML($html);
        $result['missing_alt_tags'] = $this->checkImageAltTag($doc->getElementsByTagName('img'));
        $result['robots_meta_tag'] = $this->getRobotsMetaTag($doc->getElementsByTagName('meta'));

        return response()->view('results.show', ['result' => $result]);
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getInsightsData($url)
    {
        $url = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' . $url;
        $response = Http::get($url);
        return json_decode($response->body());
    }

    /**
     * @param $url
     * @return array
     */
    public function getData($url)
    {
        $response = Http::withOptions([
            'Accept-Encoding' => 'gzip',
            'decode_content' => 'gzip',
            'version' => 2.0,
        ])->get($url);
        $x_tag = $response->getHeader('X-Robots-Tag');
        $result = [
            'checked_url' => $url,
            'status' => $response->getStatusCode(),
            'protocol' => $response->getProtocolVersion(),
            'gzip' => $this->checkGzipSupport($response->getHeader('x-encoded-content-encoding')),
            'x_robots_tag' => $x_tag == [] ? false : true,
        ];
        return $result;
    }

    public function checkGzipSupport($content_encoding)
    {
        if (array_search('gzip', $content_encoding) !== false) {
            return 'Supported';
        } else {
            return 'Not supported';
        }
    }

    /**
     * @param $images
     * @return array
     */
    public function checkImageAltTag($images)
    {
        $arr = [];
        foreach ($images as $tag) {
            if ($tag->getAttribute('alt') == '') {
                $ar = [
                    'img' => $tag->getAttribute('src'),
                ];
                array_push($arr, $ar);
            }
        }

        return $arr;

    }

    /**
     * @param $metas
     * @return string
     */
    public function getRobotsMetaTag($metas)
    {
        foreach ($metas as $meta) {
            if ($meta->getAttribute('name') == 'robots') {
                return $meta->getAttribute('content');
            }
        }
        return 'Missing';
    }
}
